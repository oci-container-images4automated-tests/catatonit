defmodule Buildah.Pacman.Catatonit do

    alias Buildah.{Pacman, Cmd} # Print

    @catatonit_cmd "catatonit"

    def catatonit_cmd() do @catatonit_cmd end

    def on_container(container, options) do
        Pacman.packages_no_cache(container, [
            "catatonit"
        ], options)
        {catatonit_exec_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v " <> @catatonit_cmd]
        )
        {_, 0} = Cmd.config(container,
            entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\"]"
        )
    end

    def test(container, image_ID, options) do
        IO.puts("Buildah PATH (maybe do not appear):")
        {catatonit_exec_, 0} = Cmd.run(container, ["printenv",
            "PATH"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        IO.puts("Podman PATH (maybe do not appear):")
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv",
            "PATH"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Cmd.run(container, [@catatonit_cmd, "--version"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, [@catatonit_cmd, "--version"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        Pacman.packages_no_cache(container, ["psmisc"], options)
        {catatonit_exec_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v pstree"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Cmd.run(container, [@catatonit_cmd, "--", "pstree"], into: IO.stream(:stdio, :line))
        {_, _} = Podman.Cmd.run(image_ID, ["/bin/sh", "-c",
            "ls -l /usr/sbin/pstree ; command -v /usr/sbin/pstree ; command -v pstree"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        ) ############################ return value
        {_, 0} = Cmd.run(container, ["whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, _} = Podman.Cmd.run(image_ID, ["/usr/sbin/pstree"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        ) ############################ return value
        # Maybe a problem with pstree...
    end

end

